import sys
import os
import requests
import urllib3
from urllib3 import exceptions
from datetime import datetime, timedelta
currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self, parser_name: str):
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_data()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def get_data(self):
        date_format = '%d.%m.%Y %H:%M'
        today = datetime.now()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)

        from_date: str = yesterday.strftime(date_format)
        to_date: str = tomorrow.strftime(date_format)
        print('date from: {}, to: {}'.format(from_date, to_date))

        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()
        url = 'https://eshoprzd.ru/rest/elsearch/api/v1/searchPurchase/uetp'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://etp.comita.ru/', verify=False)

        json_data = {"itemsPerPage": 10, "orderAsc": False, "orderBy": "startDate", "page": 0, "search": ""}
        response = session.post(url, json=json_data, verify=False)
        data = response.json()
        contents = self.get_content(data, yesterday, tomorrow)
        return contents

    def get_content(self, data, yesterday, tomorrow):
        orders = []
        for item in data['items']:

            date = datetime.fromtimestamp(item.get("created") / 1000)
            if yesterday < date < tomorrow:
                item_data = {
                    'type': 2,
                    'title': '',
                    'purchaseNumber': '',
                    'fz': 'Коммерческие',
                    'purchaseType': '',
                    'url': '',
                    'lots': [],
                    'attachments': [],
                    'procedureInfo': {
                        'endDate': ''
                    },
                    'customer': {
                        'fullName': '',
                        'factAddress': '',
                        'inn': 0,
                        'kpp': 0,
                    },
                    'contactPerson': {
                        'lastName': '',
                        'firstName': '',
                        'middleName': '',
                        'contactPhone': '',
                        'contactEMail': '',
                    }}
                item_data['title'] = str(self.get_title(item))
                item_data['purchaseNumber'] = str(self.get_purchase_number(item))
                item_data['purchaseType'] = str(self.get_purchase_type(item))

                item_data['customer']['factAddress'] = str(self.get_adress(item))
                item_data['customer']['fullName'] = str(self.get_full_name(item))
                item_data['customer']['inn'] = int(self.get_inn(item))

                item_data['procedureInfo']['endDate'] = self.get_end_date(item)

                last_name, first_name, middle_name = self.get_name(item)
                item_data['contactPerson']['lastName'] = str(last_name)
                item_data['contactPerson']['firstName'] = str(first_name)
                item_data['contactPerson']['middleName'] = str(middle_name)

                phone, email = self.get_contacts(item)
                item_data['contactPerson']['contactPhone'] = str(phone)
                item_data['contactPerson']['contactEMail'] = str(email)

                item_data['url'] = 'https://etp.comita.ru/openProcedure/' + str(item.get("type")["id"]) + '/' + str(
                    item.get("id")) + '#top'
                link = ''.join(
                    'https://etp.comita.ru/openProcedure/' + str(item.get("type")["id"]) + '/' + str(item.get("id"))).replace(
                    'openProcedure', 'rest')

                item_data['lots'] = self.get_lots(item)

                item_data['attachments'] = self.get_files(link)

                orders.append(item_data)
        return orders

    def get_title(self, data):
        try:
            title = ' '.join(data.get("name").split())
        except:
            title = ''
        return title

    def get_purchase_type(self, data):
        try:
            purchase_type = data.get("type")["name"]
        except:
            purchase_type = ''
        return purchase_type

    def get_purchase_number(self, data):
        try:
            number = data.get("number")
        except:
            number = ''
        return number

    def get_end_date(self, data):
        try:
            date = datetime.fromtimestamp(data.get("claimEnds") / 1000).strftime("%d.%m.%y")
            formatted_date = self.formate_date(date)
        except:
            formatted_date = None
        return formatted_date

    def get_adress(self, data):
        try:
            adress_data: str | None = data.get("customer")['regAddress']['addressAsString']
            if adress_data is None:
                address = ''
            else:
                address = ''.join(adress_data)
        except:
            address = ''
        return address

    def get_full_name(self, data):
        try:
            name = data.get("customer")["name"]
        except:
            name = ''
        return name

    def get_inn(self, data):
        try:
            inn = data.get("customer")["inn"]
            if not inn or inn is None:
                inn = 0
        except:
            inn = 0
        return inn

    def get_name(self, data):
        try:
            last_name_data: dict | None = data.get("contact", None)
            if last_name_data is None:
                last_name = ''
            else:
                last_name = last_name_data.get('lastName')
        except:
            last_name = ''

        try:
            first_name_data: dict | None = data.get("contact", None)
            if first_name_data is None:
                first_name = ''
            else:
                first_name = first_name_data.get('firstName')
        except:
            first_name = ''

        try:
            middle_name_data: dict | None = data.get("contact", None)
            if middle_name_data is None:
                middle_name = ''
            else:
                middle_name = middle_name_data.get('middleName')
        except:
            middle_name = ''

        return last_name, first_name, middle_name

    def get_contacts(self, data):
        contact_data: dict | None = data.get('contact')
        if isinstance(contact_data, dict):
            try:
                phone = contact_data.get('phone')
            except:
                phone = ''
            try:
                email = contact_data.get('email')
            except:
                email = ''
        else:
            phone = ''
            email = ''
        return phone, email

    def get_lots(self, data):
        try:
            lots_data = []
            lots = {
                'region': '',
                'price': '',
                'lotItems': []
            }
            for lot in data.get("lots"):
                adress = lot.get("deliveryPlaceAddress")
                if adress is None:
                    lots['region'] = ''
                else:
                    lots['region'] = ' '.join(adress.split())

                price = lot.get("priceNoNDS")
                if price is None:
                    lots['price'] = ''
                else:
                    lots['price'] = str(price)

                names = {
                    'code': str(lot.get("id")),
                    'name': str(lot.get("name"))}

                lots['lotItems'].append(names)
            lots_data.append(lots)
        except:
            return []
        return lots_data


    def get_files(self, link):
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
        session = requests.Session()
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56'
        })
        session.get('https://etp.comita.ru/', verify=False)
        response_page = session.get(link, verify=False)
        data_page = response_page.json()
        docs = []
        try:
            data = data_page.get("result")["files"]
            if data is None:
                doc = {'docDescription': '', 'url': ''}
                docs.append(doc)
            else:
                for file in data:
                    doc = {'docDescription': str(file.get("fileName")), 'url': str(file.get("url"))}
                    docs.append(doc)
        except:
            return []
        return docs

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%y')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
